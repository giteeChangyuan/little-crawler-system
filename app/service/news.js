'use strict';

const Service = require('egg').Service;

class NewsService extends Service {
  async getNewsList() {
    const api = this.config.api + 'appapi.php?a=getPortalList&catid=20&page=1';
    const response = await this.ctx.curl(api);
    const data = JSON.parse(response.data);
    const newsList = data.result;
    // console.log(newsList);
    return newsList;
  }
  async getNewsContent(aid) {
    const api = this.config.api + 'appapi.php?a=getPortalArticle&aid=' + aid;
    const response = await this.ctx.curl(api);
    const data = JSON.parse(response.data);
    return data.result;
  }
}

module.exports = NewsService;
