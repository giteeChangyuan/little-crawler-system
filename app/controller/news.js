'use strict';

const Controller = require('egg').Controller;

class NewsController extends Controller {
  async index() {
    const newsList = await this.service.news.getNewsList();
    await this.ctx.render('news', {
      newsList,
    });
  }
  async content() {
    const aid = this.ctx.query.aid;
    const newsContentList = await this.service.news.getNewsContent(aid);
    const title = newsContentList[0].title;
    const newsContent = newsContentList[0].content;
    console.log(title);
    await this.ctx.render('newsContent', {
      title,
      newsContent,
    });
  }
}

module.exports = NewsController;
